VERSION = $(shell grep '^version' galaxy.yml | awk '{print $$2}')
COLLECTION = arensb-qpkg-${VERSION}.tar.gz

RM = rm -f

all:	collection

# Before ansible 2.10, 'ansible-galaxy collection build' puts every
# file in the current directory into the collection. Version 2.10
# introduces the 'build_ignore' section in galaxy.yml.
#
# In the meantime, we work around this by putting the collection's
# files in the 'build/' directory, then creating the collection from
# that.
collection:	${COLLECTION}

${COLLECTION}:	galaxy.yml collection.manifest
	if [ ! -d build ]; then mkdir build; fi
	rsync -avi --files-from collection.manifest --delete ./ build/
	ansible-galaxy collection build build/

clean::
	$(RM) arensb-qpkg-*.tar.gz
	$(RM) -r build/

publish-dev:	collection
	ansible-galaxy collection publish -s galaxy_dev ${COLLECTION}

publish-prod:	collection
	ansible-galaxy collection publish -s galaxy_prod ${COLLECTION}
