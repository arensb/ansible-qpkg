# Ansible Collection - arensb.qpkg
Install, upgrade, and upgrade QPKG packages under QTS on QNAP devices.

## Installation
The easiest way to install this is to get it from Ansible Galaxy:

    ansible-galaxy collection install arensb.qpkg

Alternately, `git clone` from a git repository to a directory where
Ansible looks for collections.

## Playbook examples
    - name: Install qpkg packages
      hosts: qnap-hosts
      become: yes
      collections:
        - arensb.qpkg
      tasks:
        - name: Install packages
          qpkg:
            package:
              - Python3
              - Kodi18
              - phpMyAdmin
            state: present

    - name: Upgrade qpkg package
      hosts: qnap-hosts
      become: yes
      collections:
        - arensb.qpkg
      tasks:
        - name: Upgrade a package
          qpkg:
            package: Kodi18
            state: latest

    - name: Remove an installed package
      hosts: qnap-hosts
      become: yes
      collections:
        - arensb.qpkg
      tasks:
        - name: Remove a package
          qpkg:
            package: Python
            state: absent

## Limitations

`qpkg_cli` does not give correct results when run as an ordinary user, so
`qpkg` must be run with `become: yes`, even in check mode.
