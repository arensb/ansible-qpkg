#!/usr/bin/env python3
"""Manage QPKG packages on QTS devices."""

from ansible.module_utils.basic import AnsibleModule
import time
import configparser

# Copyright: (C) 2023, Andrew Arensburger
# GNU General Public License v3.0+ (see COPYING or
# https://www.gnu.org/licenses/gpl-3.0.txt)

# XXX - Maybe add a 'base' or 'base_url' parameter, to allow
# installing multiple packages from the same site:
#       qpkg:
#         name:
#           - foo
#           - bar
#           - baz
#         base_url: https://my.site/path/to/repo/
#
# would download
# - https://my.site/path/to/repo/foo.qpkg
# - https://my.site/path/to/repo/bar.qpkg
# - https://my.site/path/to/repo/baz.qpkg

# XXX - qpkg_cli supports -P <path> and -I <volume> saying which
# directory/volume to install to. Add parameters for this.

# XXX - What about --update_dependency, which updates all dependent
# packages? Should this be on by default? Probably want a module
# argument to turn it on or off.

ANSIBLE_METADATA = {
    'metadata_version': '1.1',
    'status': ['preview'],
    'supported_by': 'community'
}

DOCUMENTATION = '''
---
module: qpkg

short_description: Manage qpkg packages

version_added: "2.9"

description:
  - "Installs and removes `qpkg' packages for QTS on QNAP devices."
options:
  package:
    description:
      A package name, like C(foo), or a list of packages.
    required: true
    aliases: [ pkg, name ]
  state:
    description:
      Desired package state.
      If C(present), the package should be installed.
      If C(latest), the package should be installed and upgraded to the
      latest version.
      if C(absent), the package should be removed.
    required: false
    default: present

notes:
    This fails if not run as root, so `become: true' is required.

author:
    - Andrew Arensburger (@arensb)
'''

EXAMPLES = '''
# Install a package
- name: Install a package
  qpkg:
    name: XBMC
    state: present

# Install multiple pacakges
- name: Install base packages
  qpkg:
    name:
      - XBMC
      - Kodi18
      - PlexMediaServer
    state: present

# Remove a package
- name: Uninstall a package
  qpkg:
    name: XBMC
    state: absent

# Make sure that the latest version of a package is installed
- name: Upgrade a package
  qpkg:
    name: XBMC
    state: latest
'''

# XXX - Fix this
RETURN = '''
message:
    description: The output message that the test module generates
    type: str
    returned: always
'''

DRAIN_TIMEOUT = 600     # Timeout waiting for queue to drain, in seconds.
DRAIN_WAIT_TIME = 10    # How long to wait between queue checks.

# packages: cache of packages.
# Cache the "Is there an upgrade?" information about packages.
packages = None


def list_packages(module):
    # XXX - Should this accept list of packages?
    """Return a list of installed packages.

    As a side effect, sets C(packages), which also says whether they can
    be upgraded.
    """
    global packages
    if packages is not None:
        # We've already looked it up.
        return packages.keys()

    err, stdout, stderr = module.run_command(['/sbin/qpkg_cli', '-G', 'all'])
    if err != 0:
        # Failure
        # XXX - Should we just abort the module?
        return None

    # `stdout` contains ini-code. Parse it into a dictionary.
    ini = configparser.ConfigParser()
    err = ini.read_string(stdout, source="/sbin/qpkg_cli -G all")

    packages = {}

    # There is one ini-section per package, named after the package.
    # And each section contains an 'upgradeStatus' entry, an integer.
    for s in ini.sections():
        packages[s] = {'upgradeStatus': ini.get(s, 'upgradeStatus')}

    # But all we care about here are the package names. So return that.
    return packages.keys()


def is_installed(pkg):
    """Return true iff C(pkg) is installed.

    Convenience function. Returns true iff C(pkg) is installed,
    according to C(packages).
    """
    global packages
    return pkg in packages


def is_upgradable(pkg):
    """Return true iff C(pkg) is installed and upgradable.

    Convenience function. Returns true iff C(pkg) is installed and
    upgradable, according to C(packages).
    """
    global packages
    return (pkg in packages) and \
        (packages[pkg]['upgradeStatus'] == "0")


def install(module, pkg):
    """Install package C(pkg) using qpkg_cli.

    Returns a tuple: (status, data) where C(status) is a boolean indicating
    success, and C(data) is a dictionary with additional information.
    Currently, this includes C(stdout) and C(stderr) from C(qpkg_cli).
    """
    if module.check_mode:
        return None, {}             # Okay

    # Tell qpkg to install the package.
    # Unfortunately, this will only tell the daemon to start the
    # installation. Now we have to run flush_queue to wait for it to finish.
    err, stdout, stderr = module.run_command(['/sbin/qpkg_cli', '-a', pkg])
    if err != 0:
        # Failure
        return False, {
            'stdout': stdout,
            'stderr': stderr
            }
    return True, {
            'stdout': stdout,
            'stderr': stderr
        }


def remove(module, pkg):
    """Uninstall a package.

    Tells qpkg to uninstall a package. You will need to use some other
    method to make sure the uninstallation terminated.

    Returns a tuple: (status, data) where C(status) is a boolean indicating
    success, and C(data) is a dictionary with additional information.
    Currently, this includes C(stdout) and C(stderr) from C(qpkg_cli).

    """
    if module.check_mode:
        # result['message'] += "Ought to pretend-remove %s\n" % pkg
        return None, {}

    # Tell qpkg to uninstall the package
    # Unfortunately, this will only tell the daemon to start the
    # installation. Now we have to run flush_queue to wait for it to finish.
    err, stdout, stderr = module.run_command(['/sbin/qpkg_cli', '-R', pkg])
    if err != 0:
        # Failure
        return False, {
            'stdout': stdout,
            'stderr': stderr
            }
    return True, {
            'stdout': stdout,
            'stderr': stderr
        }


def pkg_status(module, pkg):
    """Get the installation status of C(pkg).

    Returns a tuple: (err, stdout, stderr) where C(err) is the exit
    status of qpkg_cli -s C(pkg) (0 if successful), C(stdout) and
    C(stderr) are its stdout and stderr, respectively. Upon success,
    C(stdout) has the package installation status, and upon failure,
    C(stderr) has an error message.

    Useful statuses include:
    - <pkg> is installed
    - <pkg> not found
    Other statuses may appear while the package is being installed or removed,
    including:
    - <pkg> is queuing
    - <pkg> download 100%
    - <pkg> is in installation stage 99
    - <pkg> is in installation stage 2 (or 3)
    - <pkg> opcode 5 status code 4
    """
    err, stdout, stderr = module.run_command(['/sbin/qpkg_cli', '-s', pkg])
    return err, stdout, stderr


def drain_queue(module, pkgs):
    """Wait for all of the packages in C(pkgs) to leave the job queue.

    Returns a tuple: (<status>, <output>) where C(status) is either
    True for success and False for failure, and C(output) is a
    dictionary with keys C(stdout) and C(stderr), which hopefully
    contain interesting information.
    """
    # Note: it's possible for a package to remain in the queue (and in
    # /etc/config/qpkg_job.conf) indefinitely, even after it has been
    # fully installed.

    # XXX - Problem: some time can pass between running qpkg_cli
    # to add/remove a package, and the job actually showing up in
    # the queue.
    #
    # The elegant way to fix this would be to monitor the queue to
    # make sure that a job appears, then wait until the package is
    # installed (as reported by 'qpkg_cli -s <pkg>') (and the reverse
    # when uninstalling a package).
    #
    # For now, we'll just wait a few seconds for jobs to appear in the
    # queue, then start waiting for them to disappear.

    retval = {
        'stdout': '',
        'stderr': '',
    }

    # Give the job time to appear in the queue
    time.sleep(5)

    start_time = time.time()
    while True:
        done = True

        # Go through the list of packages we care about, and see what
        # state they're in: either installed/uninstalled, or something
        # else.
        for p in pkgs:
            # Check the package status
            stat_err, stat_stdout, stat_stderr = pkg_status(module, p)

            if stat_err != 0:
                retval['stdout'] += stat_stdout
                retval['stderr'] += stat_stderr
                return False, retval

            # If qpkg_cli -s <pkg> says either
            #   QPKG <pkg> is installed
            # or
            #   QPKG <pkg> not found
            # then it's either installed or not installed. Anything
            # else means it's in flux or something.
            if stat_stdout.find(" is installed") > 0 or \
               stat_stdout.find(" not found") > 0:
                pass
            # XXX - Maybe look for
            # [CLI] recv error, code = -1
            # and consider that a transient error. If it occurs 3 times,
            # give up.
            else:
                # Anything else presumably means that the package is
                # in flux.
                done = False
                break

        if done:
            # We're done here
            return True, retval

        # If we get this far, at least one of the packages we care
        # about is still in the queue.
        now = time.time()
        if now >= start_time + DRAIN_TIMEOUT:
            # Timeout. Stop waiting.
            retval['status'] = "Timeout"
            return False, retval

        # Wait a bit and try again
        time.sleep(DRAIN_WAIT_TIME)


def main():
    """Initialize and run Ansible qpkg module."""
    # Define things about this module:
    # - Supported arguments, their types, and defaults.
    # - Whether check mode is supported.
    module = AnsibleModule(
        # define available arguments/parameters a user can pass to the module
        argument_spec=dict(
            state=dict(type='str',
                       required=False,
                       default='present',
                       choices=['absent', 'latest', 'present'],
                       ),
            package=dict(type='list',
                         required=True,
                         aliases=['pkg', 'name'],
                         ),
        ),
        supports_check_mode=True,
    )

    # seed the result dict in the object
    # we primarily care about changed and state
    # change is if this module effectively modified the target
    # state will include any data that you want your module to pass back
    # for consumption, for example, in a subsequent task
    result = dict(
        changed=False,
        changes=[],
        message=''
    )

    # Get the list of installed packages.
    # Put it in the result, so we can see them with '-vvv', and find out
    # what packages' IDs are.
    result['packages'] = list_packages(module)

    # Go through each package named in the parameters, and decide what
    # to do about it.
    for pkg in module.params['package']:

        if module.params['state'] == 'present':
            if not is_installed(pkg):
                result['changes'].append("install %s" % pkg)
                result['changed'] = True
                err, pkg_result = install(module, pkg)
        elif module.params['state'] == 'latest':
            if (not is_installed(pkg)) or is_upgradable(pkg):
                result['changes'].append("upgrade %s" % pkg)
                result['changed'] = True
                err, pkg_result = install(module, pkg)
        elif module.params['state'] == 'absent':
            if is_installed(pkg):
                result['changes'].append("remove %s" % pkg)
                result['changed'] = True
                err, pkg_result = remove(module, pkg)
        else:
            # This should never happen.
            module.fail_json(msg="Unrecognized state: %s" %
                             module.params['state'])

    # Wait for the queue to drain
    if not module.check_mode:
        # XXX - Handle errors during draining (e.g., timeout)

        err, data = drain_queue(module, module.params['package'])
        if not err:
            module.fail_json(msg="Error draining queue: {%s}" % data)
        # if 'stdout' in data:
        #     result['message'] += "drain_queue stdout: %s" % data['stdout']
        # if 'stderr' in data:
        #     result['message'] += "drain_queue stderr: %s" % data['stderr']

    # during the execution of the module, if there is an exception or a
    # conditional state that effectively causes a failure, run
    # AnsibleModule.fail_json() to pass in the message and the result
    # if module.params['package'] == 'fail me':
    #     module.fail_json(msg='You requested this to fail', **result)

    # in the event of a successful module execution, you will want to
    # simple AnsibleModule.exit_json(), passing the key/value results
    module.exit_json(**result)


if __name__ == '__main__':
    main()
